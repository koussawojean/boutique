/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Achat;

/**
 *
 * @author jean
 */
public class AchatClient {
    private static AchatClient INSTANCE;
    private String URL = "http://localhost:8080/boutique-web/api/client";
    
    private AchatClient() {
        
    }
    
    public static synchronized AchatClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AchatClient();
        }
        return INSTANCE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.URL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AchatClient other = (AchatClient) obj;
        return Objects.equals(this.URL, other.URL);
    }

    @Override
    public String toString() {
        return "AchatClient{" + "URL=" + URL + '}';
    }
    
    public Achat trouver(int id) {
        
        Client client = ClientBuilder.newClient();
        Achat ach = null;
        Response response = client.target(this.URL)
                .path(String.valueOf(id))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            // reqdEntity renvoie le retours dans le type entré en paramètre
            ach = response.readEntity(Achat.class);
            System.out.println(ach);
        } else {
            System.out.println(response.getStatus());
        }
        return ach;
    }
    
    public void ajouter(Achat a) {
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL) // url
                .request() // Création du builder avec type de par d'envoie
                .post(Entity.json(a)); 
    }
    
    public Achat modifier(Achat a) {
        Achat updatedA = null;
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(a));
        if (rep.getStatus() == 200) {
            updatedA = rep.readEntity(Achat.class);
            System.out.println("Achat modifié avec succès");
            System.out.println(updatedA);

        } else {
            System.out.println("Erreur! Status infos : \n"+ rep.getStatusInfo() +"\nCode : "+ rep.getStatus());
        }
        return updatedA;
    }
    
    public void supprimer(int id) {
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .path("/"+String.valueOf(id))
                .request()
                .delete();
    }
    
    public int compter() {
        int nombre = -1;
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .path("/nombre")
                .request()
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
            System.out.println("Nombre d'achat = "+nombre);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return nombre;
    }
    
    public List<Achat> lister() {
        List<Achat> lAch = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {
          
            lAch = response.readEntity(new GenericType<List<Achat>>(){});

            System.out.println("Liste de = "+lAch);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return lAch;
    }
    
}
