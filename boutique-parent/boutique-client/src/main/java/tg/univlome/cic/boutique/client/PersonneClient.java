/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Personne;

/**
 *
 * @author jean
 */
public class PersonneClient {
    private static PersonneClient INSTANCE;
    private String URL = "http://localhost:8080/boutique-web/api/personne";
    
    private PersonneClient() {
        
    }
    
    public static synchronized PersonneClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PersonneClient();
        }
        return INSTANCE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.URL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PersonneClient other = (PersonneClient) obj;
        return Objects.equals(this.URL, other.URL);
    }

    @Override
    public String toString() {
        return "PersonneClient{" + "URL=" + URL + '}';
    }
    
    public Personne trouver(long id) {
        
        Client client = ClientBuilder.newClient();
        Personne p = null;
        Response response = client.target(this.URL)
                .path(String.valueOf(id))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            // reqdEntity renvoie le retours dans le type entré en paramètre
            p = response.readEntity(Personne.class);
            System.out.println(p);
        } else {
            System.out.println(response.getStatus());
        }
        return p;
    }
    
    public void ajouter(Personne p) {
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL) // url
                .request() // Création du builder avec type de par d'envoie
                .post(Entity.json(p));
    }
    
    public Personne modifier(Personne p) {
        Personne updatedP = null;
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(p));
        if (rep.getStatus() == 200) {
            updatedP = rep.readEntity(Personne.class);
            System.out.println("Personne modifié avec succès");
            System.out.println(updatedP);

        } else {
            System.out.println("Erreur! Status infos : \n"+ rep.getStatusInfo() +"\nCode : "+ rep.getStatus());
        }
        return updatedP;
    }
    
    public void supprimer(long id) {
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .path("/"+String.valueOf(id))
                .request()
                .delete();
    }
    
    public int compter() {
        int nombre = -1;
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .path("/nombre")
                .request()
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
            System.out.println("Nombre de personne = "+nombre);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return nombre;
    }
    
    public List<Personne> lister() {
        List<Personne> lPersonnes = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {
          
            lPersonnes = response.readEntity(new GenericType<List<Personne>>(){});

            System.out.println("Liste de = "+lPersonnes);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return lPersonnes;
    }
    
}
