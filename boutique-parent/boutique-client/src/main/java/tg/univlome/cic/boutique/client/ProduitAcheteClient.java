/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.ProduitAchete;

/**
 *
 * @author jean
 */
public class ProduitAcheteClient {
    private static ProduitAcheteClient INSTANCE;
    private String URL = "http://localhost:8080/boutique-web/api/produit-achete";
    
    private ProduitAcheteClient() {
        
    }
    
    public static synchronized ProduitAcheteClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProduitAcheteClient();
        }
        return INSTANCE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.URL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAcheteClient other = (ProduitAcheteClient) obj;
        return Objects.equals(this.URL, other.URL);
    }

    @Override
    public String toString() {
        return "ProduitAchete{" + "URL=" + URL + '}';
    }
    
    public ProduitAchete trouver(int idAchat, long idProduit) {
        
        Client client = ClientBuilder.newClient();
        ProduitAchete produitAch = null;
        Response response = client.target(this.URL+"?idachat="+String.valueOf(idAchat)+"&idproduit="+idProduit)
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            // reqdEntity renvoie le retours dans le type entré en paramètre
            produitAch = response.readEntity(ProduitAchete.class);
            System.out.println(produitAch);
        } else {
            System.out.println(response.getStatus());
        }
        return produitAch;
    }
    
    public void ajouter(ProduitAchete p) {
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL) // url
                .request() // Création du builder avec type de par d'envoie
                .post(Entity.json(p)); // Méthode
        /*if (response.getStatus() == 200) {
            System.out.println("Objet envoyé avec succès");
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }*/
    }
    
    public ProduitAchete modifier(ProduitAchete p) {
        ProduitAchete updatedP = null;
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(p));
        if (rep.getStatus() == 200) {
            updatedP = rep.readEntity(ProduitAchete.class);
            System.out.println("ProduitAchete modifié avec succès");
            System.out.println(updatedP);

        } else {
            System.out.println("Erreur! Status infos : \n"+ rep.getStatusInfo() +"\nCode : "+ rep.getStatus());
        }
        return updatedP;
    }
    
    public void supprimer(int idAchat, long idProduit) {
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL+"?idachat="+String.valueOf(idAchat)+"&idproduit="+idProduit)
                .request()
                .delete();
    }
    
    public int compter() {
        int nombre = -1;
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .path("/nombre")
                .request()
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
            System.out.println("Nombre de Produits Achetés = "+nombre);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return nombre;
    }
    
    public List<ProduitAchete> lister() {
        List<ProduitAchete> lProduitsAch = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {
          
            lProduitsAch = response.readEntity(new GenericType<List<ProduitAchete>>(){});

            System.out.println("Liste de = "+lProduitsAch);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return lProduitsAch;
    }
    
}
