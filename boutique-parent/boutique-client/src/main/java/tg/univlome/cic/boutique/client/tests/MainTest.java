/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package tg.univlome.cic.boutique.client.tests;

import tg.univlome.cic.boutique.client.ClientClient;
import tg.univlome.cic.boutique.client.ProduitAcheteClient;

/**
 *
 * @author jean
 */
public class MainTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ClientClient pc = ClientClient.getInstance();
        System.out.println(pc.compter());
        ProduitAcheteClient pAch = ProduitAcheteClient.getInstance();
        System.out.println(pAch.trouver(0, 0));
    }
    
}
