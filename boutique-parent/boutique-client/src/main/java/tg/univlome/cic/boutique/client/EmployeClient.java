/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Employe;

/**
 *
 * @author jean
 */
public class EmployeClient {
    private static EmployeClient INSTANCE;
    private String URL = "http://localhost:8080/boutique-web/api/employe";
    
    private EmployeClient() {
        
    }
    
    public static synchronized EmployeClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EmployeClient();
        }
        return INSTANCE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.URL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmployeClient other = (EmployeClient) obj;
        return Objects.equals(this.URL, other.URL);
    }

    @Override
    public String toString() {
        return "EmployeClient{" + "URL=" + URL + '}';
    }
    
    public Employe trouver(long id) {
        
        Client client = ClientBuilder.newClient();
        Employe employe = null;
        Response response = client.target(this.URL)
                .path(String.valueOf(id))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            // reqdEntity renvoie le retours dans le type entré en paramètre
            employe = response.readEntity(Employe.class);
            System.out.println(employe);
        } else {
            System.out.println(response.getStatus());
        }
        return employe;
    }
    
    public void ajouter(Employe p) {
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL) // url
                .request() // Création du builder avec type de par d'envoie
                .post(Entity.json(p)); // Méthode
        /*if (response.getStatus() == 200) {
            System.out.println("Objet envoyé avec succès");
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }*/
    }
    
    public Employe modifier(Employe p) {
        Employe updatedE = null;
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(p));
        if (rep.getStatus() == 200) {
            updatedE = rep.readEntity(Employe.class);
            System.out.println("Employe modifié avec succès");
            System.out.println(updatedE);

        } else {
            System.out.println("Erreur! Status infos : \n"+ rep.getStatusInfo() +"\nCode : "+ rep.getStatus());
        }
        return updatedE;
    }
    
    public void supprimer(long id) {
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .path("/"+String.valueOf(id))
                .request()
                .delete();
    }
    
    public int compter() {
        int nombre = -1;
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .path("/nombre")
                .request()
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
            System.out.println("Nombre d'employes = "+nombre);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return nombre;
    }
    
    public List<Employe> lister() {
        List<Employe> lE = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {
          
            lE = response.readEntity(new GenericType<List<Employe>>(){});

            System.out.println("Liste de employers = "+lE);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return lE;
    }
    
}
