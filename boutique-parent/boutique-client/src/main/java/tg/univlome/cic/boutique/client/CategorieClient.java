/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import tg.univlome.cic.boutique.entites.Categorie;

/**
 *
 * @author jean
 */
public class CategorieClient {
    private static CategorieClient INSTANCE;
    private String URL = "http://localhost:8080/boutique-web/api/categorie";
    
    private CategorieClient() {
        
    }
    
    public static synchronized CategorieClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new CategorieClient();
        }
        return INSTANCE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.URL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CategorieClient other = (CategorieClient) obj;
        return Objects.equals(this.URL, other.URL);
    }

    @Override
    public String toString() {
        return "CategorieClient{" + "URL=" + URL + '}';
    }
    
    public Categorie trouver(long id) {
        
        Client client = ClientBuilder.newClient();
        Categorie c = null;
        Response response = client.target(this.URL)
                .path(String.valueOf(id))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            // reqdEntity renvoie le retours dans le type entré en paramètre
            c = response.readEntity(Categorie.class);
            System.out.println(c);
        } else {
            System.out.println(response.getStatus());
        }
        return c;
    }
    
    public void ajouter(Categorie c) {
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL) // url
                .request() // Création du builder avec type de par d'envoie
                .post(Entity.json(c));
    }
    
    public Categorie modifier(Categorie c) {
        Categorie newC = null;
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(c));
        if (rep.getStatus() == 200) {
            newC = rep.readEntity(Categorie.class);
            System.out.println("Categorie modifié avec succès");
            System.out.println(newC);

        } else {
            System.out.println("Erreur! Status infos : \n"+ rep.getStatusInfo() +"\nCode : "+ rep.getStatus());
        }
        return newC;
    }
    
    public void supprimer(long id) {
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .path("/"+String.valueOf(id))
                .request()
                .delete();
    }
    
    public int compter() {
        int nombre = -1;
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .path("/nombre")
                .request()
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
            System.out.println("Nombre de categories = "+nombre);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return nombre;
    }
    
    public List<Categorie> lister() {
        List<Categorie> lCat = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {
          
            lCat = response.readEntity(new GenericType<List<Categorie>>(){});

            System.out.println("Liste de = "+lCat);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return lCat;
    }
    
}
