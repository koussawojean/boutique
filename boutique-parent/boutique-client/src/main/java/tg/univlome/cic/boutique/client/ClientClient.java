/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jean
 */
public class ClientClient {
    private static ClientClient INSTANCE;
    private String URL = "http://localhost:8080/boutique-web/api/client";
    
    private ClientClient() {
        
    }
    
    public static synchronized ClientClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClientClient();
        }
        return INSTANCE;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.URL);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClientClient other = (ClientClient) obj;
        return Objects.equals(this.URL, other.URL);
    }

    @Override
    public String toString() {
        return "ClientClient{" + "URL=" + URL + '}';
    }
    
    public tg.univlome.cic.boutique.entites.Client trouver(long id) {
        
        Client client = ClientBuilder.newClient();
        tg.univlome.cic.boutique.entites.Client cl = null;
        Response response = client.target(this.URL)
                .path(String.valueOf(id))
                .request(MediaType.APPLICATION_JSON_TYPE)
                .get();
        if (response.getStatus() == 200) {
            // reqdEntity renvoie le retours dans le type entré en paramètre
            cl = response.readEntity(tg.univlome.cic.boutique.entites.Client.class);
            System.out.println(cl);
        } else {
            System.out.println(response.getStatus());
        }
        return cl;
    }
    
    public void ajouter(tg.univlome.cic.boutique.entites.Client cl) {
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL) // url
                .request() // Création du builder avec type de par d'envoie
                .post(Entity.json(cl)); // Méthode
        /*if (response.getStatus() == 200) {
            System.out.println("Objet envoyé avec succès");
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }*/
    }
    
    public tg.univlome.cic.boutique.entites.Client modifier(tg.univlome.cic.boutique.entites.Client cl) {
        tg.univlome.cic.boutique.entites.Client updatedC = null;
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(cl));
        if (rep.getStatus() == 200) {
            updatedC = rep.readEntity(tg.univlome.cic.boutique.entites.Client.class);
            System.out.println("Client modifié avec succès");
            System.out.println(updatedC);

        } else {
            System.out.println("Erreur! Status infos : \n"+ rep.getStatusInfo() +"\nCode : "+ rep.getStatus());
        }
        return updatedC;
    }
    
    public void supprimer(long id) {
        Client client = ClientBuilder.newClient();
        Response rep = client.target(this.URL)
                .path("/"+String.valueOf(id))
                .request()
                .delete();
    }
    
    public int compter() {
        int nombre = -1;
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .path("/nombre")
                .request()
                .get();
        if (response.getStatus() == 200) {
            nombre = response.readEntity(int.class);
            System.out.println("Nombre de clients = "+nombre);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return nombre;
    }
    
    public List<tg.univlome.cic.boutique.entites.Client> lister() {
        List<tg.univlome.cic.boutique.entites.Client> lClient = new ArrayList<>();
        Client client = ClientBuilder.newClient();
        Response response = client.target(this.URL)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {
          
            lClient = response.readEntity(new GenericType<List<tg.univlome.cic.boutique.entites.Client>>(){});

            System.out.println("Liste de clients = "+lClient);
        } else {
            System.out.println("Erreur! Status infos : \n"+ response.getStatusInfo() +"\nCode : "+ response.getStatus());
        }
        return lClient;
    }
    
}
