/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.resources;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import tg.univlome.cic.boutique.web.service.ProduitService;
import tg.univlome.cic.boutique.entites.Produit;

/**
 *
 * @author jean
 */
@Path("produit")
public class ProduitResource {
    
    private ProduitService service;

    public ProduitResource() {
        this.service = ProduitService.getInstance();
    }

    
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public GenericEntity<List<Produit>> lister() {
        GenericEntity<List<Produit>> genEnListProd = new GenericEntity<List<Produit>>(this.service.lister()){};
        return genEnListProd;
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Produit trouver(@PathParam("id") long id) {
        return this.service.trouver(id);
    }

    @GET
    @Path("/nombre")
    @Produces(MediaType.TEXT_PLAIN)
    public int compter() {
        return this.service.compter();
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void ajouter(Produit p) {
        this.service.ajouter(p);
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void modifier(Produit p) {
        this.service.modifier(p);
    }

    @DELETE
    @Path("/{id}")
    public void supprimer(@PathParam("id") long id) {
        this.service.supprimer(id);
    }
}
