/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.service;

import java.util.ArrayList;
import tg.univlome.cic.boutique.entites.Employe;

/**
 *
 * @author jean
 */
public class EmployeService {
    static private ArrayList<Employe> liste = new ArrayList<>();
    static private EmployeService INSTANCE = new EmployeService();

    private EmployeService() {
    }

    public static synchronized EmployeService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new EmployeService();
        }
        return INSTANCE;
    }

    public int getIndexInList(long clientId) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).getId() == clientId) {
                return i;
            }
        }
        return -1;
    }

    public void ajouter(Employe e) {
        liste.add(e);
    }

    public void modifier(Employe e) {
        int i;
        i = liste.indexOf(e);
        if (i >= 0) {
            liste.set(i, e);
        }
    }

    public void supprimer(long id) {
        liste.remove(getIndexInList(id));
    }

    public ArrayList<Employe> lister() {
        return liste;
    }

    public Employe trouver(long id) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).getId() == id) {
                return liste.get(i);
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }
}
