/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.service;

import java.util.ArrayList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Personne;

/**
 *
 * @author jean
 */
public class PersonneService {
    private static List<Personne> liste = new ArrayList<>();
    private static PersonneService INSTANCE;

    private PersonneService() {
        
    }

    public static synchronized PersonneService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new PersonneService();
        }
        return INSTANCE;
    }

    public void ajouter(Personne e) {
        liste.add(e);
    }

    public void modifier(Personne e) {
        int i;
        i = liste.indexOf(e);
        if (i >= 0) {
            liste.set(i, e);
        }
    }

    public void supprimer(long id) {
        for(Personne p : liste) {
            if(p.getId() == id){
                liste.remove(p);
            }
        }
    }

    public List<Personne> lister() {
        return liste;
    }

    public Personne trouver(long id) {
        for (Personne produit : liste) {
            if (produit.getId()==id) {
                return produit;
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }
}
