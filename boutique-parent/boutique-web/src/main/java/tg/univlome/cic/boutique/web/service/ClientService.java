/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.service;

import java.util.ArrayList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Client;

/**
 *
 * @author jean
 */
public class ClientService {

    private static List<Client> liste = new ArrayList<>();
    private static ClientService INSTANCE = new ClientService();

    private ClientService() {
    }

    public static synchronized ClientService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ClientService();
        }
        return INSTANCE;
    }

    public int getIndexInList(long clientId) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).getId() == clientId) {
                return i;
            }
        }
        return -1;
    }

    public void ajouter(Client e) {
        liste.add(e);
    }

    public void modifier(Client e) {
        int i;
        i = liste.indexOf(e);
        if (i >= 0) {
            liste.set(i, e);
        }
    }

    public void supprimer(long id) {
        liste.remove(getIndexInList(id));
    }

    public List<Client> lister() {
        return liste;
    }

    public Client trouver(long id) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).getId() == id) {
                return liste.get(i);
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }

}
