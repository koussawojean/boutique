/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.service;

import java.util.ArrayList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Achat;
import tg.univlome.cic.boutique.entites.Produit;
import tg.univlome.cic.boutique.entites.ProduitAchete;

/**
 *
 * @author jean
 */
public class ProduitAcheteService {
    static private List<ProduitAchete> liste = new ArrayList<>();
    static private ProduitAcheteService INSTANCE;

    private ProduitAcheteService() {
    }

    public static synchronized ProduitAcheteService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProduitAcheteService();
        }
        return INSTANCE;
    }

    public int getIndexInList(ProduitAchete prodAch) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).equals(prodAch)) {
                return i;
            }
        }
        return -1;
    }

    public void ajouter(ProduitAchete e) {
        liste.add(e);
    }

    public void modifier(ProduitAchete e) {
        int i;
        i = liste.indexOf(e);
        if (i >= 0) {
            liste.set(i, e);
        }
    }

    public List<ProduitAchete> lister() {
        return liste;
    }
    
    public ProduitAchete trouver(int idAchat, int idProduit) {
        for (ProduitAchete produitAchete : liste) {
            if (produitAchete.getAchat().getId() == idAchat && produitAchete.getProduit().getId() == idProduit) {
                return produitAchete;
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }

    public void supprimer(int idAchat, int idProduit) {
        for (ProduitAchete produitAchete : liste) {
            if (produitAchete.getAchat().getId() == idAchat && produitAchete.getProduit().getId() == idProduit) {
                liste.remove(produitAchete);
            }
        }
    }

}
