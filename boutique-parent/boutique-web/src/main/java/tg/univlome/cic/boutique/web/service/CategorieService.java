/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.service;

import java.util.ArrayList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Categorie;

/**
 *
 * @author jean
 */
public class CategorieService {
    private static CategorieService INSTANCE;
    private List<Categorie> liste = new ArrayList<>();
    
    private CategorieService() {
        liste.add(new Categorie(1, "Chaussure", "Mocassin, Paire, ..."));
        liste.add(new Categorie(2, "Robe", "Mocassin, Paire, ..."));
        liste.add(new Categorie(3, "Chemise", "Vêtements du dessus..."));
        liste.add(new Categorie(4, "Pantalon", "Toi mm tu sais ..."));
    }
    
    public static synchronized CategorieService getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new CategorieService();
        }
        return INSTANCE;
    }

    public List<Categorie> getListe() {
        return liste;
    }

    public void setListe(List<Categorie> liste) {
        this.liste = liste;
    }

    public int getIndexInList(long id) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    public void ajouter(Categorie c) {
        liste.add(c);
    }

    public void modifier(Categorie e) {
        int i;
        i = liste.indexOf(e);
        if (i >= 0) {
            liste.set(i, e);
        }
    }

    public void supprimer(long id) {
        liste.remove(getIndexInList(id));
    }

    public List<Categorie> lister() {
        return liste;
    }

    public Categorie trouver(long id) {
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).getId() == id) {
                return liste.get(i);
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }
    
    
}
