/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.resources;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import tg.univlome.cic.boutique.entites.ProduitAchete;
import tg.univlome.cic.boutique.web.service.ProduitAcheteService;

/**
 *
 * @author jean
 */
@Path("produit-achete")
public class ProduitAcheteResource {
    private ProduitAcheteService service;

    public ProduitAcheteResource() {
        this.service = ProduitAcheteService.getInstance();
    }

    
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public GenericEntity<List<ProduitAchete>> lister() {
        GenericEntity<List<ProduitAchete>> genEnListProd = new GenericEntity<List<ProduitAchete>>(this.service.lister()){};
        return genEnListProd;
    }

    @POST
    @Path("trouver")    
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public ProduitAchete trouver(@QueryParam("idachat") int idAchat, @QueryParam("idproduit") int idProduit) {
        return this.service.trouver(idAchat, idProduit);
    }

    @GET
    @Path("/nombre")
    @Produces(MediaType.TEXT_PLAIN)
    public int compter() {
        return this.service.compter();
    }

    @POST
    @Path("ajouter")
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void ajouter(ProduitAchete p) {
        this.service.ajouter(p);
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void modifier(ProduitAchete p) {
        this.service.modifier(p);
    }

    @DELETE
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void supprimer(@QueryParam("idachat") int idAchat, @QueryParam("idproduit") int idProduit) {
        this.service.supprimer(idAchat, idProduit);
    }
}
