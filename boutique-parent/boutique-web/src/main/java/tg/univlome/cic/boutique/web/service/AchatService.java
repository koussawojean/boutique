/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.service;

import java.util.ArrayList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Achat;

/**
 *
 * @author jean
 */
public class AchatService {
    private static List<Achat> liste = new ArrayList<>();
    private static AchatService INSTANCE;

    private AchatService() {
        
    }

    public static synchronized AchatService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AchatService();
        }
        return INSTANCE;
    }

    public void ajouter(Achat e) {
        liste.add(e);
    }

    public void modifier(Achat e) {
        int i;
        i = liste.indexOf(e);
        if (i >= 0) {
            liste.set(i, e);
        }
    }

    public void supprimer(long id) {
        for(Achat p : liste) {
            if(p.getId() == id){
                liste.remove(p);
            }
        }
    }

    public List<Achat> lister() {
        return liste;
    }

    public Achat trouver(int id) {
        for (Achat produit : liste) {
            if (produit.getId()==id) {
                return produit;
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }
}
