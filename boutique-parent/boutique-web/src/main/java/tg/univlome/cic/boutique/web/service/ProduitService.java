/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.web.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import tg.univlome.cic.boutique.entites.Categorie;
import tg.univlome.cic.boutique.entites.Produit;

/**
 *
 * @author jean
 */
public class ProduitService {

    private static List<Produit> liste = new ArrayList<>();
    private static ProduitService INSTANCE;

    private ProduitService() {
        Categorie c1 = CategorieService.getInstance().getListe().get(0);
        Categorie c2 = CategorieService.getInstance().getListe().get(1);
        liste.add(new Produit(1l, "Fils", 1000.0, LocalDate.of(2025, 12, 1), c1));
        liste.add(new Produit(2l, "Adidas", 2500.0, LocalDate.of(2030, 12, 1), c1));
        liste.add(new Produit(3l, "Zara", 10000.0, LocalDate.of(2035, 12, 4), c2));
    }

    public static synchronized ProduitService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ProduitService();
        }
        return INSTANCE;
    }

    public void ajouter(Produit e) {
        liste.add(e);
    }

    public void modifier(Produit e) {
        int i;
        i = liste.indexOf(e);
        if (i >= 0) {
            liste.set(i, e);
        }
    }

    public void supprimer(long id) {
        for(Produit p : liste) {
            if(p.getId() == id){
                liste.remove(p);
            }
        }
    }

    public List<Produit> lister() {
        return liste;
    }

    public Produit trouver(long id) {
        for (Produit produit : liste) {
            if (produit.getId()==id) {
                return produit;
            }
        }
        return null;
    }

    public int compter() {
        return liste.size();
    }

}
