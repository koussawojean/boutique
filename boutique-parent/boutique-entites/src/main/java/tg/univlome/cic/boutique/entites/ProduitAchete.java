/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.entites;

import java.io.Serializable;

/**
 *
 * @author jean
 */
public class ProduitAchete implements Serializable{
    private int quantite = 1;
    private double remise = 0.0;
    private Produit produit;
    private Achat achat;

    public ProduitAchete(Produit produit) {
        this.produit = produit;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public ProduitAchete() {
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public Achat getAchat() {
        return achat;
    }

    public void setAchat(Achat achat) {
        this.achat = achat;
    }
    
    public double getPrixTotal() {
        return (this.produit.getPrixUnitaire()*this.quantite)*(1-this.remise);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + this.quantite;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProduitAchete other = (ProduitAchete) obj;
        return this.quantite == other.quantite;
    }
    
    
}
