/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tg.univlome.cic.boutique.entites;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jean
 */
public class Achat implements Serializable {

    private int id;
    private LocalDateTime dateAchat;
    private double remise = 0.0;
    private ArrayList<ProduitAchete> produitAchete = new ArrayList<>(); 
    private Client client;
    private Employe employe;

    public Achat() {
    }

    public Achat(int id, LocalDateTime dateAchat, Client client, Employe employe) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.client = client;
        this.employe = employe;
    }

    public Achat(int id, LocalDateTime dateAchat, ArrayList<ProduitAchete> produitAchete, Client client, Employe employe) {
        this.id = id;
        this.dateAchat = dateAchat;
        this.produitAchete = produitAchete;
        this.client = client;
        this.employe = employe;
    }

    

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public List<ProduitAchete> getProduitAchete() {
        return produitAchete;
    }

    public void setProduitAchete(ArrayList<ProduitAchete> produitAchete) {
        this.produitAchete = produitAchete;
    }

   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDateTime dateAchat) {
        this.dateAchat = dateAchat;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        return this.id == other.id;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", dateAchat=" + dateAchat + ", remise=" + remise + ", produitAchete=" + produitAchete + ", client=" + client + ", employe=" + employe + '}';
    }

    // Methodes propres
    public double getRemiseTotal() {
        double remTotal = 0.0;
        for(ProduitAchete prodAch : produitAchete) {
            remTotal += prodAch.getPrixTotal() * remise;
        }
        return remTotal;
    }

    public double getPrixTotal() {
        double prixTotal = 0.0;
        for(ProduitAchete prodAch : produitAchete) {
            prixTotal += prodAch.getPrixTotal() * (1 - remise);
        }
        return prixTotal;
    }

}
